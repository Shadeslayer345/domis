import math

def exponen_inverse(a, y):
  return (math.log(1 - y) / -a)

def exponen(a, x):
  return 1 - math.exp(-a*x)

def geometric(p, u):
  return math.log(u) / math.log(1 - p)

def weibull_inverse(a, b, y):
  return 2 * math.pow(-1 * math.log(1 - y), (1/a))

def weibull(a,b,x):
  return 1 - math.exp(-(x / b)** a)

def main():
  #
  # Weibull example
  #
  #low_b = 1
  #up_b = 5
  #u = 0.3

  # For truncated range
  #v = weibull(2, 2, low_b) + (weibull(2, 2, up_b) - weibull(2, 2, low_b)) * u
  #x = weibull_inverse(2, 2, v)


  #
  # Exponential example
  #

  print(geometric(0.26, 0.5))

if __name__ == '__main__':
  main()
