import numpy as np
import scipy as sp
import scipy.stats as stats

def confidence_interval(a, confidence):
  m = np.mean(a)
  z = stats.norm.ppf(1-confidence)
  var = np.var(a, ddof=1)
  interval_val = z * np.sqrt(var / len(a))

  return m, m + interval_val, m - interval_val

def t_confidence_interval(a, confidence):
  m = np.mean(a)
  t = stats.t.ppf(confidence, len(a) - 1)
  print(t)
  var = np.var(a, ddof=1)
  interval_val = t * np.sqrt(var / len(a))

  return m, m + interval_val, m - interval_val


def skewness(a, bias):
  skew = stats.skew(a, bias)

  if (skew < 1) or (skew > -1):
    return "Symmetric", skew

  return "Not Symmetric", skew

def test_null_hy(a, mu, alpha):
  m = np.mean(a)
  var = np.var(a, ddof=1)

  test_val = (m - mu) / np.sqrt(var / len(a))

  if test_val < alpha:
    return test_val, alpha, "Cannot Reject"

def main():
  a = [1.94, 1.02, -7.03, 1.42, 3.44, 4.91, -0.3, 2.34, 1.26, -0.05, 3.63, 7.32, -3.23, 4.35, -0.45, -0.24, 1.13, 3.52, 2.01, 5.23, 1.1, 1.78, 3.4, 5.32, 6.21, 1.55, -3.14, 8.6, 1.57, 4.84]
  print(test_null_hy(a, 2.1, 0.1))
  pass

if __name__ == '__main__':
  main()
