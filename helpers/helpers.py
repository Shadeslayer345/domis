import numpy as np
import scipy as sp
import scipy.stats as stats

# Get a (used in Willink Confidence Interval - see chapter 4 slide 50)
# est_skewness: estimated skewness
# variance: variance
# total_values: total number of values
def a(est_skewness, variance, total_values):
  alpha = est_skewness / (6 * (total_values)**0.5 * (variance)**1.5)
  print("a value is: {}".format(alpha))
  return alpha

# Get a_incorrect (used in Willink Confidence Interval - equation from midterm)
# est_skewness: estimated skewness
# variance: variance
# total_values: total number of values
def a_incorrect(est_skewness, variance, total_values):
  alpha = est_skewness / (6 * (total_values)**0.5)
  print("(Incorrect) a value is: {}".format(alpha))
  return alpha

# Calculate Willink Confidence interval
# a: from a()
# t_value: corresponding t-value
# avg: average
# variance: variance
# arr_size: size of list

def willink_confidence(a, t_value, avg, variance, arr_size):
  g_pos = (((1+6 * a * (t_value - a))**(1./3.)) - 1)/(2 * a)

  g_neg = (((1+6 * a * (-t_value - a))**(1./3.)) - 1)/(2 * a)
  print("G(r) is: %f " % g_pos)

  print("G(-r) is: %f " % g_neg)

  willink_pos = avg - g_pos * (variance / arr_size)**0.5
  print("Willink Positive: {}".format(willink_pos))

  willink_neg = avg - g_neg * (variance / arr_size)**0.5
  print("Willink Negative: {}".format(willink_neg))

def main():
  pass

if __name__ == '__main__':
  main()
