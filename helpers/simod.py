import math

def relative_error(data, gamma_y, estimated_mean):
  minimum = 0
  gamma = gamma_y / (1 + gamma_y)
  for x in range(1, len(data)):
    relative_error_tmp = (data[x] * math.sqrt(0.31 / x)) / estimated_mean
    if (relative_error_tmp <= gamma):
      if (minimum == 0 or x < minimum):
        minimum = x

  return minimum

def absolute_error(data, beta):
  minimum = 0
  for x in range(1, len(data)):
    absolute_error_tmp = data[x] * math.sqrt(0.31 / x)
    if (absolute_error_tmp < beta):
      if (minimum == 0 or x < minimum):
        minimum = x

  return minimum

def welch_w_m(avg_prcs_vals, i, w, vals):
  end = w + 1
  start = -w
  denom = (2 * w) + 1
  tmp = 0

  for x in range(start, end):
    tmp = tmp + avg_prcs_vals[i+x]

  return (tmp / denom)

def welch_i_w(avg_prcs_vals, i, w, vals):
  end = i
  start = -(i - 1)
  denom = (2 * i) - 1
  tmp = 0

  for s in range(start, end):
    tmp = tmp + avg_prcs_vals[i+s]

  return (tmp / denom)

def welch_procedure(averaged_process_vals, w, m):
  vals = []

  for i in range(1, w+1):
    vals.append(welch_i_w(averaged_process_vals, i, w, vals))
  for i in range(w+1, (m-w) + 1):
    vals.append(welch_w_m(averaged_process_vals, i, w, vals))

  return vals

def main():
  t_distribution_table_col_90 = [0, 3.078, 1.886, 1.638, 1.533, 1.476, 1.440, 1.415, 1.397, 1.383, 1.372, 1.363, 1.356, 1.350, 1.345, 1.341, 1.337, 1.333, 1.330, 1.328, 1.325, 1.323, 1.321, 1.319, 1.318, 1.316, 1.315, 1.314, 1.313, 1.311, 1.310, 1.303, 1.299, 1.293, 1.290, 1.282]
  t_distribution_table_col_95 = [0, 6.314, 2.920, 2.353, 2.132, 2.015, 1.943, 1.895, 1.860, 1.833, 1.812, 1.796, 1.782, 1.771, 1.761, 1.753, 1.746, 1.740, 1.734, 1.729, 1.725, 1.721, 1.717, 1.714, 1.711, 1.708, 1.706, 1.703, 1.701, 1.699, 1.697, 1.684, 1.676, 1.665, 1.660, 1.645]
  y_i = [7.1, 6.51, 6.49, 5.76, 4.92, 4.37, 4.29, 4.79, 4.21, 4.33]

  #print(relative_error(t_distribution_table_col_95, 0.10, 2.03))
  #print(absolute_error(t_distribution_table_col_95, 0.25))
  print(welch_procedure(y_i, 3, 9))

if __name__ == '__main__':
  main()
