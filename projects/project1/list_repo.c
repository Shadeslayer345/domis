#include "list_repo.h"

/**
 * @brief      Creates a memory pool bounded by the passed parameter
 *
 * @param[in]  max_element_num  Maximum amount of elements for this memory pool
 *
 * @return     The pointer to the newly created repo
 */
void * CreateRepoList(unsigned int max_element_num) {
  int pool_size = max_element_num * sizeof(Node);
  void * mem_pool = malloc(pool_size + 1);

  // Initialize the memory pool
  for (int i = 0; i < pool_size; i++) {
    ((Repo_List_Element *) mem_pool)[i].prev = 0;
    ((Repo_List_Element *) mem_pool)[i].next = 0;
    memset(((Repo_List_Element *) mem_pool)[i].data, 0, REPO_ELEMENT_DATA_SIZE);
  }

  // Create an end of list (EOR) flag node
  ((Repo_List_Element *) mem_pool)[pool_size + 1].prev = -5;

  return mem_pool;
}

/**
 * @brief      Frees all memory allocated to a repo
 *
 * @param      repo  The repo we're freeing
 *
 * @return     1 if successful
 *             2 if unsuccessful
 */

void * DestroyListRepo(void * repo) {
  if (repo == NULL) {
    return 0;
  }

  free(repo);
  return 1;
}

/**
 * @brief      Creates a list.
 *
 * @param      repo  The repo we're creating the list in
 * @param      tail  Pointer to the index of the list tail
 *
 * @return     The index of the newly creted element
 */
int CreateList(void * repo, int * tail) {
  char insertIndexData[4] = "head";
  int insertIndex = getAvailableIndex(repo);

  ((Repo_List_Element *) repo)[insertIndex].prev = -2;
  ((Repo_List_Element *) repo)[insertIndex].next = -2;
  memcopy(((Repo_List_Element *) repo)[insertIndex].data, insertIndexData, 128);

  *tail = insertIndex;
  return insertIndex;
}

/**
 * @brief      Remove all elements from a list
 *
 * @param      repo  The repo
 * @param      head  The head
 *
 * @return     1 if Successful
 *             0 if Failure
 */
int DeleteList(void * repo, int head) {
  if (((Repo_List_Element *) repo)[head]->prev == -2 ) {
    return 0;
  }

  Repo_List_Element * delete_node = ((Repo_List_Element *) repo)[head];

  do {
    Repo_List_Element * temp_node = delete_node;
    delete_node = ((Repo_List_Element *) repo)[delete_node->next];
    temp_node->prev = 0;
    temp_node->next = 0;

    memset(temp_node->data, 0, 128);
    free(temp_node);
  } while(delete_node.next != -1)

  return 1;
}

/**
 * @brief      Inserting a new element into the list
 *
 * @param      repo         The repo
 * @param      head         The head
 * @param      tail         The tail
 * @param      element_ptr  The element pointer
 * @param      n            The index to insert to
 *
 * @return     1 if Successful
 *             0 if Failure
 */
int InsertElement(void * repo, int * head, int * tail, void * element_ptr, int n) {
  if (((Repo_List_Element *) repo)[*head].prev == -2) {
    ((Repo_List_Element *) repo)[*head].prev = -1;
    ((Repo_List_Element *) repo)[*head].next = -1;

    memcopy(((Repo_List_Element *) repo)[head].data, element_ptr, 128);
    return 1;
  }

  int insertIndex = getAvailableIndex(repo);
  Repo_List_Element * insertNode = ((Repo_List_Element *) repo)[insertIndex];

  memcopy(insertNode->data, element_ptr, 128);

  if (n < 0) {
    if (n == -1) {
      insertNode->next = -1;
      insertNode->prev = *tail;
      *tail = insertIndex;

      return 1;
    }

    Repo_List_Element * traverse_node = ((Repo_List_Element *) repo)[*tail];
    int i;

    for (i = n; traverse_node.prev != -1 || i < 0; i++) {
      traverse_node = ((Repo_List_Element *) repo)[traverse_node.prev];
    }

    if (i == 0) {
      insertNode->next = i;
      insertNode->prev = traverse_node->prev;
      ((Repo_List_Element *) repo)[traverse_node->prev].next = insertIndex;

      return 1;
    }

    insertNode->next = -1;
    insertNode->prev = *tail;
    ((Repo_List_Element *) repo)[tail].next = insertIndex;
    *tail = insertIndex;

    return 1;
  } else {
    if (n == 0) {
      insertNode->next = *head;
      insertNode->prev = -1;
      *head = insertIndex;

      return 1;
    }

    Repo_List_Element * traverse_node = ((Repo_List_Element *) repo)[*head];
    int i;

    for (i = n; traverse_node.next != -1 || i > 0; i--) {
      traverse_node = ((Repo_List_Element *) repo)[traverse_node.next];
    }

    if (i == 0) {
      insertNode->next = i;
      insertNode->prev = traverse_node->prev;
      ((Repo_List_Element *) repo)[traverse_node->prev].next = insertIndex;
    }

    insertNode->next = *head;
    insertNode->prev= -1;
    ((Repo_List_Element *) repo)[*head].prev = insertIndex;
    *head = insertIndex;

    return 1;
  }
}

/**
 * @brief      Remove an element from the list
 *
 * @param      repo  The repo
 * @param      head  The head
 * @param      tail  The tail
 * @param      n     Index to remove
 *
 * @return     1 if Successful
 *             0 if Failure
 */
int DeleteElement(void * repo, int * head, int * tail, int n) {
  if (((Repo_List_Element *) repo)[*head].next == -2) {
    return 0;
  }

  if (((Repo_List_Element *) repo)[*head].next == -1) {
    memset(((Repo_List_Element *) repo)[*head].data, 0, 128);
    ((Repo_List_Element *) repo)[*head].prev = -2;
    ((Repo_List_Element *) repo)[*head].next= -2;

    return 1;
  }

  if (n < 0) {
    if (n == -1) {
      memset(((Repo_List_Element *) repo)[*tail].data, 0, 128);

      int tail_prev = ((Repo_List_Element *) repo)[*tail].prev;
      ((Repo_List_Element *) repo)[tail_prev].next = -1;
      ((Repo_List_Element *) repo)[*tail].prev = -2;
      ((Repo_List_Element *) repo)[*tail].next = -2;
      *tail = tail_prev;

      return 1;
    }

    Repo_List_Element * traverse_node = ((Repo_List_Element *) repo)[*tail];
    int i;

    for (i = n; traverse_node.prev != -1 || i < 0; i++) {
      traverse_node = ((Repo_List_Element *) repo)[traverse_node.prev];
    }

    if (i == 0) {
      memset(traverse_node->data, 0, 128);

      ((Repo_List_Element *) repo)[traverse_node->prev].next =
            [traverse_node->next]
      traverse_node->prev = -2;
      traverse_node->next = -2;

      return 1;
    }

    return 0;
  } else {
    if (n == 0) {
      memset(((Repo_List_Element *) repo)[*head].data, 0, 128);

      int head_next = ((Repo_List_Element *) repo)[*head].next;
      ((Repo_List_Element *) repo)[head_next].prev = -1;
      ((Repo_List_Element *) repo)[*head].prev = -2;
      ((Repo_List_Element *) repo)[*head].next = -2;
      *head = head_next;

      return 1;
    }

    Repo_List_Element * traverse_node = ((Repo_List_Element *) repo)[*head];
    int i;

    for (i = n; traverse_node.next != -1 || i > 0; i--) {
      traverse_node = ((Repo_List_Element *) repo)[traverse_node.next];
    }

    if (i == 0) {
      memset(traverse_node->data, 0, 128);

      ((Repo_List_Element *) repo)[traverse_node->prev].next =
            [traverse_node->next]
      traverse_node->prev = -2;
      traverse_node->next = -2;

      return 1;
    }

    return 0;
  }

  return 0;
}

/**
 * @brief      Retrieve an element from the list
 *
 * @param      repo  The repo
 * @param      head  The head
 * @param      tail  The tail
 * @param      n     The index we're grabbing
 *
 * @return     Return the value at that index or Null
 */
void * RetrieveElement(void * repo, int head, int tail, int n) {
  if (((Repo_List_Element *) repo)[head].next == -2) {
    return NULL;
  }

  if (n < 0) {
    if (n == -1) {
      return (void *) ((Repo_List_Element *) repo)[tail];
    }

    Repo_List_Element * traverse_node = ((Repo_List_Element *) repo)[tail];
    int i;

    for (i = n; traverse_node.prev != -1 || i < 0; i++) {
      traverse_node = ((Repo_List_Element *) repo)[traverse_node.prev];
    }

    if (i == 0) {
      return (void * ) traverse_node;
    }

  } else {
    if (n == 0) {
      return (void *) ((Repo_List_Element *) repo)[head];
    }

    Repo_List_Element * traverse_node = ((Repo_List_Element *) repo)[head];
    int i;

    for (i = n; traverse_node.next != -1 || i > 0; i--) {
      traverse_node = ((Repo_List_Element * ) repo)[traverse_node.next];
    }

    if (i == 0) {
      return (void *) traverse_node;
    }
  }

  return NULL;
}

/**
 * @brief      Traverse the memory pool for an available index.
 *
 * @param      repo  The repo we're traversing
 *
 * @return     An available index in the repo if there's space
 *             -1 if no space is available
 */
static int getAvailableIndex(void * repo) {
  for (int i = 0; ; i++) {
    Repo_List_Element * traverse_node = ((Repo_List_Element *)repo)[i];

    // EOR
    if (traverse_node.prev == -5) {
      return -1;
    }

    // An empty index
    if (traverse_node.prev == -2) {
      return i;
    }
  }
}

/**
 * @brief      Gets the last index for any given repo.
 *
 * @param      repo  The repo we're traversing
 *
 * @return     The last index in the repo
 *             -1 if no space is available
 */
static int getLastIndex(void * repo) {
  for (int i = 0; ; i++) {
    Repo_List_Element * traverse_node = ((Repo_List_Element *)repo)[i];

    if (traverse_node.prev == -5) {
      return i;
    }
  }

  return -1;
}
