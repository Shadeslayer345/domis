#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef _DOMIS_PROJECTS_PROJECT1_List_Repo_H
#define _DOMIS_PROJECTS_PROJECT1_List_Repo_H

#define REPO_ELEMENT_DATA_SIZE 128

struct Repo_List_Element {
  char data[REPO_ELEMENT_SIZE];
  int prev;
  int next;
};

// Constructors & Destructors
void * CreateRepoList(unsigned int max_element_num);
int DestroyListRepo(void * repo);
void * CreateList();
void * DeleteList();
// Mutators
int InsertElement(int * head, int * tail, void * element_ptr, int n);
int DeleteElement(int * head, int * tail, int n);
// Accessors
void * RetrieveElement(int head, int tail, int n);
//Helpers
static int getNextAvailableIndex(void * repo);
static int getLastIndex(void * repo);

#endif
