"""M/M/C input generator

Generates samples for
  a) exponential distribution

"""
from textwrap import wrap
from numpy.random import exponential

def get_exp_dist_vals_w_mean(scale=1, size=10):
  """Return exponential distribution random vals

  Args:
      scale (int, optional): Expected mean of distribution sample
      size (int, optional): Expected size of distribution sample

  Returns:
      list: The sample data
  """
  return exponential(scale, size)

def main():
  """ Runnner

  Writes results of sample distribution to a file
  """

  inter_arrival_times = get_exp_dist_vals_w_mean(3, 100)
  service_times = get_exp_dist_vals_w_mean(5, 100)

  f_one = open('inter_arrival_times.txt', 'w+')
  wrapped = wrap(', '.join(format((x * 10), '.0f') for x in inter_arrival_times),
                 width=15)
  for line in wrapped:
    f_one.write(line)
  f_one.close()

  f_two = open('service_times.txt', 'w+')
  wrapped = wrap(', '.join(format((x * 10), '.0f') for x in service_times))
  for line in wrapped:
    f_two.write(line)
  f_two.close()

if __name__ == '__main__':
  main()
