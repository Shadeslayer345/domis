/**
 * @file    lcgrand.h
 * @author  brwnrclse (Barry Harris)
 * @date    5 December 2016
 * @version 0.1
 * @brief   M/M/C queuing system
 *          @see http://mhhe.com/law
*/

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <time.h>
#include "lcgrand/lcgrand.hpp"

#define Q_LIMIT 100
#define BUSY 1
#define IDLE 0
#define SEED_MAX 2147483646

int next_event_type,
    num_custs_delayed,
    num_events,
    num_in_q,
    server_one_status,
    server_two_status;

float area_num_in_q,
      area_server_one_status,
      area_server_two_status,
      mean_interarrival,
      mean_service,
      sim_time,
      time_arrival[Q_LIMIT + 1],
      time_end,
      time_last_event,
      time_next_event[5],
      total_of_delays;

std::ifstream infile;
std::ofstream outfile;

void  initialize(void);
void  timing(void);
void  arrive(void);
void  depart_one(void);
void  depart_two(void);
void  report(void);
void  update_time_avg_stats(void);
float expon(float mean);

/**
 * @brief      The main simulation code. Input is read in from the mmc.in file
 *                   and the simulation begins. Each "turn" the
 *
 * @return     0 if successful
 */
int main(int argc, char const *argv[]) {
  /* Open input and output files. */
  infile.open("mmc.in");
  outfile.open("mmc.out");

  /* Seed the random number generator */
  srand(time(NULL));
  long seed = rand() % SEED_MAX;
  lcgrandst(seed, 1);
  std::cout << seed << std::endl << std::endl;

  /* The simulation will have 4 events: arrivals(1) and departures(2) and end */
  num_events = 4;

  infile >> mean_interarrival >> mean_service >> time_end;
  infile.close();

  outfile << "Two-server queueing system\n\n"
          << "Mean interarrival time " << std::setprecision(4) << mean_interarrival
          << " minutes" << std::endl << std::endl
          << "Mean service time " << std::setprecision(4) << mean_service
          << " minutes" << std::endl << std::endl
          << "Length of the simulation " << time_end << std::endl << std::endl;

  initialize();

  do {
    timing();
    update_time_avg_stats();

    switch(next_event_type) {
      case 1:
        arrive();
        break;
      case 2:
        depart_one();
        break;
      case 3:
        depart_two();
        break;
      case 4:
        report();
        break;
    }
  } while (next_event_type != 4);

  outfile.close();

  return 0;
}

/**
 * @brief      Initialize the simulation, state variables, stat counters and
 *                   event list.
 */
void initialize(void) {
  sim_time = 0.0;

  server_one_status = IDLE;
  server_two_status = IDLE;
  num_in_q = 0;
  time_last_event = 0.0;

  num_custs_delayed = 0;
  total_of_delays = 0.0;
  area_num_in_q = 0.0;
  area_server_one_status = 0.0;
  area_server_two_status = 0.0;

  /* Since no customers are present, the departures are not considered. */
  time_next_event[1] = sim_time + expon(mean_interarrival);
  time_next_event[2] = 1.0e+30;
  time_next_event[3] = 1.0e+30;
  time_next_event[4] = time_end;
}


/**
 * @brief      Our timing functions.
 */
void timing(void) {
  int i;
  float min_time_next_event = 1.0e+29;

  next_event_type = 0;

  /* Determine the event type of the next event to occur. */
  for (i = 1; i <= num_events; ++i) {
    if (time_next_event[i] < min_time_next_event) {
      min_time_next_event = time_next_event[i];
      next_event_type = i;
    }
  }

  /* If the event list is empty, we'll notify the user and stop the sim. */
  if (next_event_type == 0) {
    outfile << std::endl << "Event list empty at time " << sim_time << std::endl;
    exit(1);
  }

  /* Advance the simulation clock if the event list still has values */
  sim_time = min_time_next_event;
}

/**
 * @brief      Arrival event function.
 *             The next arrival is scheduled and each server is checked to
 *                   see if it is available to begin servicing.
 *
 *             If both servers are busy then the queue is updated,
 *                   overflow is checked, and the arrival time is logged.
 *
 *             If one server is available then that server begins working.
 *                   The delayed customers is incremented and a new departure is
 *                   scheduled.
 */
void arrive(void) {
  time_next_event[1] = sim_time + expon(mean_interarrival);

  if (server_one_status == BUSY && server_two_status == BUSY) {
    ++num_in_q;

    /* When a the queue has overflowed, stop the sim */
    if (num_in_q > Q_LIMIT) {
      outfile << std::endl << "Overflow of the queue time_arrival at "
              << " time " << sim_time;
      exit(2);
    }

    time_arrival[num_in_q] = sim_time;
  } else if (server_one_status == IDLE) {
    total_of_delays += 0.0;

    ++num_custs_delayed;

    server_one_status = BUSY;
    time_next_event[2] = sim_time + expon(mean_service);
  } else if (server_two_status == IDLE) {
    total_of_delays += 0.0;

    ++num_custs_delayed;

    server_two_status = BUSY;
    time_next_event[3] = sim_time + expon(mean_service);
  }
}

/**
 * @brief      Update the simulation in response to a departure from server one.
 *
 *             The total delay accumulator is updated, the number of
 *                   customers delayed is incremented and a new departure is
 *                   scheduled.
 */
void depart_one(void) {
  if (num_in_q == 0) {
    server_one_status = IDLE;
    time_next_event[2] = 1.0e+30;
  } else {
    --num_in_q;

    total_of_delays += sim_time - time_arrival[1];

    ++num_custs_delayed;
    time_next_event[2] = sim_time + expon(mean_service);

    for (int i = 1; i <= num_in_q; ++i) {
      time_arrival[i] = time_arrival[i + 1];
    }
  }
}

/**
 * @brief      Update the simulation in response to a departure from server two.
 *
 *             The total delay accumulator is updated, the number of
 *                   customers delayed is incremented and a new departure is
 *                   scheduled.
 */
void depart_two(void) {
  if (num_in_q == 0) {
    server_two_status = IDLE;
    time_next_event[3] = 1.0e+30;
  } else {
    --num_in_q;

    total_of_delays += sim_time - time_arrival[1];

    ++num_custs_delayed;
    time_next_event[3] = sim_time + expon(mean_service);

    for (int i = 1; i <= num_in_q; ++i) {
      time_arrival[i] = time_arrival[i + 1];
    }
  }
}

/**
 * @brief      Report generator function. Compute and write estimes of desired
 *                   measures of performance.
 */
void report(void) {
  outfile << std::endl << std::endl
          << "Average delay in queue " << std::setprecision(3)
          << (total_of_delays / num_custs_delayed) << " minutes"
          << std::endl << std::endl
          << "Average number in queue " << std::setprecision(3)
          << (area_num_in_q / sim_time) << std::endl << std::endl
          << "Server One Utilization " << std::setprecision(3)
          << (area_server_one_status / sim_time) << std::endl << std::endl
          << "Server Two Utilization " << std::setprecision(3)
          << (area_server_one_status / sim_time) << std::endl << std::endl
          << "Number of delays completed " << std::setprecision(3)
          << num_custs_delayed << std::endl;
}

/**
 * @brief      Update area accumulators for time-average statistics.
 */
void update_time_avg_stats(void) {
  float time_since_last_event;

  /* Compute time since last event, and update last-event-time marker. */
  time_since_last_event = sim_time - time_last_event;
  time_last_event = sim_time;

  /* Update area under number-in-queue function. */
  area_num_in_q += num_in_q * time_since_last_event;

  /* Update area under server-busy indicator function. */
  area_server_one_status += server_one_status * time_since_last_event;
  area_server_two_status += server_two_status * time_since_last_event;
}

/**
 * @brief      Exponential variate generation function.
 *
 * @param[in]  mean  The mean
 *
 * @return     An exponential random variate
 */
float expon(float mean) {
  return -mean * log(lcgrand(1));
}
