/**
 * @file    lcgrand.h
 * @author  brwnrclse (Barry Harris)
 * @date    5 December 2016
 * @version 1.0
 * @brief   Prime modulus multiplicative linear congruential generator based on
 *                Marse and Roberts' portable FORTRAN random-number generator
 *                UNIRAN. Multiple streams are supported, up to 100, with seeds
 *                spaced 100,000 apart.
 *                @see http://mhhe.com/law
 *
 *          Z[i] = (630360016 * Z[i - 1]) * (mod(pow(2,31) -1))
*/

/**
 * @brief      Generate the next  U(0, 1) random number from the given stream.
 *
 * @param[in]  stream  The stream
 *
 * @return     The next random number
 */
float lcgrand(int stream);

/**
 * @brief      Set the current seed for the given stream to a desired zset.
 *
 * @param[in]  zset    Value to seed, from 1 to 2147483646
 * @param[in]  stream  The stream to modify
 */
void  lcgrandst(long zset, int stream);

/**
 * @brief      To get the current (most recently used) integer in the sequence.
 *
 * @param[in]  stream  The desired stream number
 *
 * @return     The current integer in the sequence as a long
 */
long  lcgrandgt(int stream);
