# Project 2

## Using the code
The simulation code is located at `mm2.cpp` and the random number generator is located at `lcgrand/lcgrand.cpp`. The simulation seeds the random number generator using a timestamp that is printed to the console on each run to verify uniqueness.

A second simulation was created to facilitate more servers, this is located at `mm3.cpp`.

To run the simulation code use the provided makefile:

```bash
make mm2

# Or

make mm3

# Run the application
./mmc
1226107538 # Seed Value

# Check the results
cat mmc.out

```

## Analysis
Using the outputs we can analyze the output.

| Run | Avg Wait in Queue (minutes) |
|-----|-----------------------------|
| 1   | 13.5                        |
| 2   | 7.98                        |
| 3   | 32.4                        |
| 4   | 5.95                        |
| 5   | 3.93                        |
| 6   | 4.16                        |
| 7   | 8.98                        |
| 8   | 19                          |
| 9   | 14.1                        |
| 10  | 9.48                        |
| **Average** | 11.948 |

From this we can see that sometimes the customers are happy as the queue wait time is <= 5 minutes. But this is not often enough as the average queue wait time is 11.948.

So customers aren't very happy currently.

### Trying with 3 servers
With the code updated to 3 servers we can see the wait times dramatically decreae so that in every simulation customers are happy (all wait times are < 5minutes)

| Run | Avg Wait in Queue (minutes) |
|-----|-----------------------------|
| 1   | 1.89                        |
| 2   | 1.22                        |
| 3   | 0.924                       |
| 4   | 3.16                        |
| 5   | 1.61                        |
| 6   | 4.01                        |
| 7   | 0.55                        |
| 8   | 0.967                       |
| 9   | 1.13                        |
| 10  | 0.86                        |
| **Average** | 1.632 |

### Other Stats
Other than mean queue wait time we also track these statistics per simulation  (they're also seen in each output file as well).

* Average Number in Queue
* Server Utilization (for each server)
* Number of Delays


