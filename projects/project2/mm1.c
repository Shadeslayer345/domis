#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

#define MEAN_ARRIVAL_TIME 1.0
#define MEAN_S_TIME 0.5
#define NUM_OF_CUSTOMERS 1000

float getExponential(float x);
float * getArrivalTimes(float * a_times);
float * getServiceTimes(float * a_times, float * s_times);
float * getDelayTimes(float * a_times, float * s_times, float * d_times);

int main() {
  float arrival_times[NUM_OF_CUSTOMERS] = {0};
  float service_times[NUM_OF_CUSTOMERS] = {0};
  float delay_times[NUM_OF_CUSTOMERS] = {0};

  arrival_times = getArrivalTimes(arrival_times);
  service_times = getServiceTimes(service_times);
  delay_times = getDelayTimes(delay_times);

  return 0;
}

float getExponential(float x) {
  srand((unsigned) time(NULL));
  double r;

  do {
    r = (double) (rand() / RAND_MAX);
  } while ((r == 0) || (r == 1));

  return ((-1/x) * log(r));
}

float * getArrivalTimes(float * times) {
  int r;

  float * temp_times = times;

  temp_times[0] = 0;

  for (int i = 1; i < NUM_OF_CUSTOMERS; i++) {
    r = rand() % 2;
    temp_times[i] = temp_times[i-1] + getExponential(MEAN_ARRIVAL_TIME);
  }

  return temp_times;
};

float * getServiceTimes(float * a_times, float * s_times) {
  int r = rand() % 2;

  float * temp_times = times;

  temp_times[0] = a_times[0];

  for (int i = 1; i < NUM_OF_CUSTOMERS; i++) {
    r = rand() % 2;
    temp_times[i] = temp_times[i-1] + getExponential(MEAN_S_TIME);
  }

  return  temp_times;
};

float * getExponential(float * a_times, float * s_times, float * d_times) {
  float * temp_times = d_times;
  temp_times[0] = 0;

  for (int i = 1; i < NUM_OF_CUSTOMERS; i++) {
    temp_times[i] = temp_times[i-1] + s_times[i] - a_times[i];
  }
}
