import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
from scipy.stats import skew
from statsmodels.distributions.empirical_distribution import ECDF

def prob_one_a(data):
  data.sort()
  ecdf = ECDF(data)

  x = np.linspace(min(data), max(data))
  y = ecdf(x)
  plt.step(x, y)

  # Set limits to have a better display
  plt.ylim([0.2, 1.05])
  plt.xlim([0, 18])

  plt.title('Empirical Distribution for data set 1')
  plt.xlabel('X')
  plt.ylabel('Cumulative Probability')
  plt.legend(['data point'], loc=0)
  plt.show()

def prob_one_b(data):
  plt.hist(data, bins='auto')
  plt.xlim([0, 17])
  plt.title('Histogram for data set 1')
  plt.show()

def prob_two_a(data):
  data.sort()
  e_dist = ECDF(data)

  _, bin_edges = np.histogram(data, bins=6)

  bin1 = []
  bin2 = []
  bin3 = []
  bin4 = []
  bin5 = []
  bin6 = []

  for x in data:
    if x < bin_edges[1]:
      bin1.append(x)
    elif x < bin_edges[2]:
      bin2.append(x)
    elif x < bin_edges[3]:
      bin3.append(x)
    elif x < bin_edges[4]:
      bin4.append(x)
    elif x < bin_edges[5]:
      bin5.append(x)
    elif x < bin_edges[6]:
      bin6.append(x)

  plt.plot(bin1, e_dist(bin1))
  plt.plot(bin2, e_dist(bin2))
  plt.plot(bin3, e_dist(bin3))
  plt.plot(bin4, e_dist(bin4))
  plt.plot(bin5, e_dist(bin5))
  plt.plot(bin6, e_dist(bin6))

  plt.title('Empirical Distribution for data set 2')

  plt.xlabel('X')
  plt.ylabel('Cumulative Probability')
  plt.legend(['group 1', 'group 2', 'group 3', 'group 4', 'group 5', 'group 6'], loc=4)
  plt.show()

def prob_two_b(data):
  count = 0
  for x in data:
    if x <= 5:
      count = count + 1
  prob = count / len(data)
  print ('{:.0%}'.format(prob))

def prob_two_c(data):
  data.sort()
  plt.hist(data, bins=6)
  plt.title('Histogram for data set 2')
  plt.show()

def prob_three(data):
  y = []
  for x in range(len(data)):
    y.append(x)

  plt.scatter(data, y)
  plt.title('Scatter Plot for data set 3')
  plt.legend(['data point'], loc=0)
  plt.show()

def get_stats(data, is_continuous):
  print('mean: {}'.format(np.mean(data)))
  print('max: {}'.format(np.max(data)))
  print('min: {}'.format(np.min(data)))
  print('variance: {}'.format(np.var(data, ddof=1)))
  if (is_continuous):
    print('coefficient of variance: {}'.format(np.std(data, ddof=1) / np.mean(data)))
  if (not is_continuous):
    print('lexis ratio: {}'.format(np.var(data, ddof=1) / np.mean(data)))
  print('skewness: {}'.format(skew(data, bias=False)))

def main():
  data_one = [1, 11, 3, 1, 2, 4, 2, 1, 1, 4, 1, 7, 2, 10, 4, 2, 2, 4, 1, 13, 2, 4, 4, 9, 1, 12, 7, 3, 9, 4, 1, 2, 3, 1, 3, 8, 16, 2, 5, 6, 4, 1, 1, 3, 13, 1, 5, 1, 7, 6]
  data_two = [2.0, 6.7, 5.7, 1.8, 2.0, 5.6, 3.3, 1.8, 1.9, 1.6, 5.1, 4.8, 4.0, 6.3, 6.6, 8.0, 2.4, 1.0, 0.6, 1.5, 7.0, 7.0, 1.4, 5.1, 1.6, 5.2, 7.0, 5.6, 5.3, 4.7, 7.0, 4.1, 6.7, 9.7, 1.5, 3.7, 6.4, 1.7, 0.4, 2.1, 5.7, 9.4, 8.9, 2.1, 4.1, 1.1, 3.8, 3.0, 2.5, 2.7]
  data_three = [3.0, 3.4, 2.7, 6.8, 7.6, 4.8, 2.2, 2.5, 0.6, 6.7, 7.8, 5.6, 7.2, 9.0, 6.1, 7.9, 8.6, 7.4, 6.5, 6.8, 6.0, 5.3, 5.6, 3.4, 3.0, 3.8, 2.6, 9.6, 10.3, 8.8, 2.4, 2.5, 1.1, 6.9, 8.6, 5.4, 2.9, 3.4, 2.3, 0.7, 1.3, -0.4, 5.5, 5.6, 4.5, 5.7, 7.2, 5.6, 6.8, 7.0, 6.8, 0.5, 0.9, -1.3]

  prob_one_a(data_one)
  prob_one_b(data_one)

  prob_two_a(data_two)
  prob_two_b(data_two)
  prob_two_c(data_two)

  prob_three(data_three)

  get_stats(data_one, False)
  print(" ")
  get_stats(data_two, True)

if __name__ == '__main__':
  main()
