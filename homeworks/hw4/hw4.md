# Homework 4
Barry Harris - @02705468

## 1.
For the hypothesized data the MLE is ~ __3.77__

*Mean*: __3.77__
*STD*: __2.79__

**Q-Q Plot**
![q1-qq](q1-qq.png)

## 2.
For the hypothesized data the MLE is ~ __2.86__

*Mean*: __2.86__
*STD*: __1.93__

**P-P Plot**
![q2-pp](q1-qq.png)

## 3.

For the hypothesized data the MLE is obtained by:

*Mean (theta)*: __1.26__

![teq](Teq.png)

```python
def getT(data, theta):
  n = len(data)
  p1 = np.log(theta)
  p2 = 0
  for x in data:
    if x > 0:
      p2 = p2 + np.log(x) / n
  return np.power((p1 - p2), -1)
```

From this we obtain 1.266 which we round up to 1.3 in order to use Table 6.21.

With T = 1.3 our alpha value is .775 and beta is 1.62.

To check our work we use this equation:

![geq](gammaeq.png)

```python

# Uses scipy & statsmodels python modules
def gammaMLECheck(data, beta, alpha):
  n = len(data)
  lhs = np.log(beta) + digamma(alpha)
  rhs = 0
  for x in data:
    if x > 0:
      rhs = rhs + np.log(x)
    else:
      rhs = rhs + np.log(1)
  rhs = rhs / n
  return abs(lhs) - abs(rhs) <= .001
```

Performing the chi-square test with the distribution yeilds:
~36.814 and a p-value of ~0.0986

## 4.
Values:

* *Mean (theta)*: 884.308
* *STD (sigma)*: 2347.950

MLE for Parameters:

* *MLE for theta*: ~4.414
* *MLE for sigma*: ~0.835
* *MLE for scale*: ~82.573

```python
def logNormMLE(data):
  n = len(data)
  thetaMLE = 0
  sigmaMLE = 0
  scaleMLE = 0

  for x in data:
    thetaMLE = thetaMLE + np.log(x)

  thetaMLE = thetaMLE / n
  scaleMLE = np.exp(thetaMLE)

  for x in data:
    sigmaMLE = np.power((np.log(x) - thetaMLE), 2)

  sigmaMLE = np.power((sigmaMLE / n), 0.5)

  return thetaMLE, sigmaMLE, scaleMLE
```

## 5.
From the data given we can determine:

Best time: 123 minutes
Average Time: 270 minutes
LowerBound for walking = 580.8 minutes (beta)
Upperbound for walking = 389.5 minutes (alpha)

We generate some random data within these bounds:


Triangle distribution vals = 415.4, 375.9, 389.5, 375.1, 389.2, 270, 251.5, 469.8, 322.4, 418, 496.4, 143.8, 200, 316, 483, 298.5, 458.3, 499.9, 123, 275.6, 363.4, 499, 299.3, 499.9, 349.6, 157.9, 135.5, 222, 246.7, 391.4, 580.8

Beta distribution vals = 405.0, 424.8, 580.8, 411.7, 403.6, 420.8, 123, 386.5, 395.8, 370.5, 402.5, 416.2, 409.7, 400.4, 386.2, 270, 380.3, 407.2, 398.3, 396.6, 403.9, 380, 389.4, 399.4, 376.8, 408.9, 419.2, 395.6, 379.1, 397.1, 389.5

generator code:

```python
minimum = 123
mean = 270
maxx = 580.8
shape_param_beta = maxx
shape_param_alpha = 389.5
trivals = []
betavals = []

for x in range(27):
  trivals.append(random.triangular(minimum, maxx))

for x in range(27):
  betavals.append(1000 * random.betavariate(shape_alpha, shape_beta))

```

