import numpy as np
from scipy.special import digamma
import scipy.stats as stats
import matplotlib.pyplot as plt
import random


def q1():
  data = [5.07, 2.70, 0.29, 1.23, 4.00, 5.18, 2.63, 1.12, 1.43, 8.27, 7.51, 2.04, 3.05, 1.50, 3.14, 4.98, 1.91, 1.54, 8.07, 6.24, 5.82, 0.71, 9.53, 4.60, 5.76, 0.71, 9.50, 1.03, 0.70, 2.78]
  # For an exponential distribution, the MLE is the reciprocal of the sample mean
  theta = np.mean(data)
  sigma = np.std(data, ddof=1)

  print("Question 1:")
  print("MLE is {}".format(theta))
  print("Mean is {}".format(theta))
  print("Std is {}".format(sigma), end="\n\n")

def q2():
  data = [3, 0, 7, 2, 6, 4, 3, 4, 3, 4, 5, 0, 0, 3, 2, 1, 0, 3, 4, 4, 7, 2, 3, 6, 2, 1, 2, 2, 1, 1, 3, 0, 3, 4, 3, 5]
  # For a Poisson distribution, the MLE is the
  theta = np.mean(data)
  sigma = np.std(data, ddof=1)

  print("Question 2:")
  print("MLE is {}".format(theta))
  print("Mean is {}".format(theta))
  print("Std is {}".format(sigma), end="\n\n")

  plt.ylim([-0.5, 7.5])
  res = stats.probplot(data, plot=plt)
  #plt.show()

def numOfZeroes(arr):
  count = 0
  for x in arr:
    if x == 0:
      count = count + 1
  return count

def getT(data, theta):
  n = len(data)
  p1 = np.log(theta)
  p2 = 0
  for x in data:
    if x > 0:
      p2 = p2 + np.log(x) / n
  return np.power((p1 - p2), -1)

def gammaMLECheck(data, beta, alpha):
  n = len(data)
  lhs = np.log(beta) + digamma(alpha)
  rhs = 0
  for x in data:
    if x > 0:
      rhs = rhs + np.log(x)
    else:
      rhs = rhs + np.log(1)
  rhs = rhs / n
  return abs(lhs) - abs(rhs) <= .001 # As good as it gets


def q3():
  data = [1.48, 0.24, 1.95, 0.20, 0.58, 1.38, 0.04, 0.55, 4.56, 3.96, 1.20, 1.79, 0.23, 0.08, 0.56, 2.11, 1.38, 0.09, 0.12, 0.21, 0.68, 0.05, 3.24, 2.52, 0.01, 2.06, 0.85, 0.76, 3.55]
  theta = np.mean(data)
  sigma = np.std(data, ddof=1)
  t = getT(data, theta)
  alpha = 0.775
  beta = theta / alpha
  chisq_val, p_val = stats.chisquare(data, ddof=1)

  print("Question 3:")
  print("Mean: {}".format(theta))
  print("T: {}".format(t))
  print("Alpha: {}".format(0.827))
  print("Beta: {}".format(beta))
  print("Equations satisfied within a .001 margin error: {}".format(gammaMLECheck(data, beta, alpha)))
  print("Chisquare_test yields: {} and {} for the p-value".format(chisq_val, p_val), end="\n\n")

def logNormMLE(data):
  n = len(data)
  thetaMLE = 0
  sigmaMLE = 0
  scaleMLE = 0

  for x in data:
    thetaMLE = thetaMLE + np.log(x)

  thetaMLE = thetaMLE / n
  scaleMLE = np.exp(thetaMLE)

  for x in data:
    sigmaMLE = np.power((np.log(x) - thetaMLE), 2)

  sigmaMLE = np.power((sigmaMLE / n), 0.5)

  return thetaMLE, sigmaMLE, scaleMLE

def q4():
  data = [159.5, 24.8, 270.7, 0.3, 23.0, 287.3, 90.2, 708.3, 3387.0, 6037.5, 909.1, 12.6, 157.1, 141.8, 1.2, 329.2, 125.0, 22.4, 12.8, 1.1, 226.7, 3.6, 710.6, 2.1, 363.8, 1.3, 18.9, 145.6, 54.6, 92.1, 4119.9, 490.5, 151.0, 359.4, 8.5, 12385.6]
  theta = np.mean(data)
  sigma = np.std(data, ddof=1)

  thetaMLE, sigmaMLE, scaleMLE = logNormMLE(data)

  print("Question 4:")
  print("Mean is: {}".format(theta))
  print("STD is: {}".format(sigma))
  print("MLE for theta is: {}".format(thetaMLE))
  print("MLE for sigma is: {}".format(sigmaMLE))
  print("MLE for scale parameter is: {}".format(scaleMLE))

def q5():
  minimum = 123
  mean = 270
  maxx = 580.8
  shape_param_beta = maxx
  shape_param_alpha = 389.5
  trivals = []
  betavals = []

  for x in range(27):
    trivals.append(random.triangular(minimum, maxx))

  for x in range(27):
    betavals.append(1000 * random.betavariate(shape_param_alpha, shape_param_beta))

  print(trivals)
  print(betavals)

def main():
  q1()
  q2()
  q3()
  q4()
  q5()

if __name__ == '__main__':
  main()
