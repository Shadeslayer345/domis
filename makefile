CC = gcc

all:
	(cd projects; make)

clean:
	(cd projects; make clean)
